
var play2State = {
    create: function(){
        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'w': Phaser.Keyboard.W,
            'a': Phaser.Keyboard.A,
            's': Phaser.Keyboard.S,
            'd': Phaser.Keyboard.D
        });

        music.stop();

        game.add.image(0, 0, 'bg');
    
        createBounders();
        createPlayer2();
        createTextsBoard2();

        status = 'running';

        voice_dead = game.add.audio('voice_dead');
        voice_jump = game.add.audio('voice_jump');
        voice_nail = game.add.audio('voice_nail');
        voice_normal = game.add.audio('voice_normal');
    },
    update:update2
    
};

function update2 () {

    // bad
    if(status == 'gameOver' && keyboard.enter.isDown)  game.state.start('menu'); 
    if(status != 'running') return;

    this.physics.arcade.collide([player1, player2], platforms, effect2);
    this.physics.arcade.collide([player1, player2], [leftWall, rightWall]);
    this.physics.arcade.collide(player1, player2);
    checkTouchCeiling2(player1);
    checkTouchCeiling2(player2);
    checkGameOver2();

    updatePlayer2();
    updatePlatforms2();
    updateTextsBoard2();

    createPlatforms2();
}

var lastTime = 0;
function createPlatforms2 () {
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform2();
        distance += 1;
    }
}

function createOnePlatform2 () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer2 () {
    player1 = game.add.sprite(200, 50, 'player1');
    player2 = game.add.sprite(150, 50, 'player2');

    setPlayerAttr2(player1);
    setPlayerAttr2(player2);
}

function setPlayerAttr2(player) {
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard2 () {
    var style = {fill: '#ff0000', fontSize: '20px'};
    var style2 = {fill: '#ff0000', fontSize: '30px'};
    text1 = game.add.text(410, 10, '', style);
    text2 = game.add.text(410, 50, '', style);
    text3 = game.add.text(50, 130, '', style2);
    text3.visible = false;
}

function updatePlayer2 () {
    if(keyboard.left.isDown) {
        player1.body.velocity.x = -200;
    } else if(keyboard.right.isDown) {
        player1.body.velocity.x = 200;
    } else {
        player1.body.velocity.x = 0;
    }

    if(keyboard.a.isDown) {
        player2.body.velocity.x = -200;
    } else if(keyboard.d.isDown) {
        player2.body.velocity.x = 200;
    } else {
        player2.body.velocity.x = 0;
    }
    setPlayerAnimate2(player1);
    setPlayerAnimate2(player2);
}

function setPlayerAnimate2(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
       player.frame = 8;
    }
}

function updatePlatforms2 () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard2 () {
    text1.setText('p1: ' + player1.life);
    text2.setText('p2: ' + player2.life);
}

function effect2(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect2(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect2(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect2(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect2(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect2(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect2(player, platform);
    }
}

function conveyorRightEffect2(player, platform) {
    player.body.x += 2;
}

function conveyorLeftEffect2(player, platform) {
    player.body.x -= 2;
}

function trampolineEffect2(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -350;
    voice_jump.play();
}

function nailsEffect2(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 3;
        player.touchOn = platform;
        game.camera.flash(0xff0000, 100);
        voice_nail.play();
    }
}

function basicEffect2(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
        voice_normal.play();
    }
}

function fakeEffect2(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
    }
}

function checkTouchCeiling2(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
            voice_nail.play();
        }
    }
}

function checkGameOver2 () {
    if(player1.life <= 0 || player1.body.y > 500) {
        voice_dead.play();
        gameOver2('player2');
    }
    if(player2.life <= 0 || player2.body.y > 500) {
        voice_dead.play();
        gameOver2('player1');
    }
}

function gameOver2(winner) {
    text3.visible = true;
    text3.setText('Winner:  ' + winner + '\n\nPress Enter to Menu !');
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    status = 'gameOver';
}
