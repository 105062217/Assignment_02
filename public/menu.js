
var UpKey;
var DownKey;
var music;
var user_name;
var database;
var scorelabel;
var scoretext;

var menuState = {
    preload: function(){
        game.load.crossOrigin = 'anonymous';
        
        game.load.spritesheet('player1', 'assets/player1.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player2.png', 32, 32);
        game.load.image('bg', 'assets/bg.jpg');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.audio('music', 'assets/music.ogg');
        game.load.audio('voice_dead', 'assets/dead.ogg');
        game.load.audio('voice_jump', 'assets/jump.ogg');
        game.load.audio('voice_nail', 'assets/nail.ogg');
        game.load.audio('voice_normal', 'assets/normal.ogg');
    },
    create: function(){
        music = game.add.audio('music');
        music.play();
        game.add.image(0, 0, 'bg');
        createBounders();

        var style = {fill: '#ff0000', fontSize: '22px'};
        game.add.text(125, 30, '小朋友下樓梯', style);
        game.add.text(45, 60, 'Press up to Play one player\nPress down to Play two player', style);
        
        UpKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        DownKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);

        game.add.text(100, 150, 'Top 5 ranking', {fill: '#fff000', fontSize: '30px'});
        // scorelabel = game.add.text(120, 200, '....', {fill: '#fff000', fontSize: '22px'});
        database = firebase.database().ref().orderByChild('score').limitToFirst(5);
        database.once('value',function(snapshot){

            scoretext = '';
            snapshot.forEach(function(childshot){
                scoretext += childshot.val().name + ' : ' + childshot.val().score*-1 + ' floors\n';
            });
            game.add.text(120, 200, scoretext, {fill: '#fff000', fontSize: '22px'});
        });
        
    },

    update: function(){
        
        if(UpKey.isDown){
            
            game.state.start('play');
            distance = 0;
            music.stop();
        }
        else if(DownKey.isDown){
            
            game.state.start('play2');
            music.stop();
        }
    }
    
};

function createBounders () {
    
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}



